#!/bin/bash

#
# (c) ShEV 2014-2021
#
# ОГРАНИЧЕНИЕ!!!
# Все компоненты виртуальной машины (виртуальные диски, логи, снимки)
# должны находиться в одной папке (пример):
#    VBoxMachine
#       |
#       +
#       <Logs>
#       <Snapshots>
#       Machine.vbox
#       Machine.vdi

if [ $1 ]
then
    # Имя виртуальной машины. Передаётся параметром
    NameVM=$1
else
    echo "Необходимо указать имя виртуальной машины"
    exit
fi

#####################################################
# !!! Переменные, которые необходимо настроить: !!! #
#####################################################
# Куда копируем. Папка, в которую копируются резервные копии
PATH_TO_VMs_ARCHIVES="/full/path/to/backup/folder/"
# Название снимка
SNAPSHOT_NAME="autobackup"
# Время хранения архивной копии (в днях)
XDAY=20
######################################################

CurrentDate=`date +%Y%m%d-%H%M`
LOG_FILE="$PATH_TO_VMs_ARCHIVES/$NameVM-$CurrentDate.log"

exec &> $LOG_FILE

echo $CurrentDate
echo "Создаём резервную копию машины - $NameVM"

######################################
# Удаляем архивные копии старше XDAY #
######################################
rm -rf `find $PATH_TO_VMs_ARCHIVES/$NameVM-* -maxdepth 1 -mtime +$XDAY`

#######################
# Обрабатываем снимки #
#######################
# Удаляем все снимки, если есть
for snap in `VBoxManage snapshot $NameVM list --machinereadable |grep ^SnapshotUUID | awk -F"=" '{sub(/^"/,"",$2);sub(/"$/,"",$2);print $2 }'`
    do
        echo "Удаляется снимок с UUID = $snap"
        VBoxManage snapshot $NameVM delete $snap
        sleep 5s
done

# Создаём снимок
echo "Создаём новый снимок $SNAPSHOT_NAME"
VBoxManage snapshot $NameVM take $SNAPSHOT_NAME --description "Automatically generated snapshot of virtual machines at $CurrentDate"

#################################
# Делаем резервную копию машины #
#################################
# Узнаём путь до виртуальной машины (имя виртуальной машины и путь до неё могут отличаться)
# Прочитаем полный путь до конфигурационного файла машины 
CfgFileVM=`VBoxManage showvminfo $NameVM --machinereadable |grep ^CfgFile | awk -F"=" '{sub(/^"/,"",$2);sub(/"$/,"",$2);print $2 }'`

# Отрезаем имя конфигурационного файла машины и оставляем только путь. Его и будем копировать.
NameFolderVM=${CfgFileVM%/*}

# Путь и имя файла, куда будет копироваться виртуальная машина
FullBackupPath=$PATH_TO_VMs_ARCHIVES$NameVM-$CurrentDate.tar.bz2

# Копируем виртуальную машину в архив tar без сжатия
echo "Архивируем виртуаьную машину $NameVM в $FullBackupPath"
sync
tar -cjf $FullBackupPath --exclude=Snapshots --exclude=Logs $NameFolderVM/

echo "Удаляем снимок $SNAPSHOT_NAME"
VBoxManage snapshot $NameVM delete $SNAPSHOT_NAME

echo "Готово!!!"
